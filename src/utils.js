import { getStore } from './store';
import mori from 'mori';

export const p = arr =>
  mori.reduce(
    (acc, cur) => {
      const a = acc ? acc + ' ' : acc;
      return a + cur;
    },
    '',
    arr
  );
export const r = p;
export const c = color => (color[0] === '#' ? color : '#' + color);

export function getIn(obj, path) {
  if (!Array.isArray(path)) path = path.split('.');
  let curElem = obj;
  for (const p of path) {
    if (typeof curElem !== 'object') return null;
    curElem = curElem[p];
  }
  return curElem;
}

function keyName(key) {
  if (typeof key === 'string') return key;
  return key.as;
}

function keyPath(key) {
  if (typeof key === 'string') return key;
  return key.path;
}

export const listenToRedux = (keys, updateFunc) => {
  let lastState = {};
  const getState = () => {
    const state = getStore().getState();
    const diff = keys
      .map(key => [keyName(key), getIn(state, keyPath(key))])
      .filter(([key, val]) => lastState[key] !== val)
      .reduce(
        (acc, [key, val]) => Object.assign(acc || {}, { [key]: val }),
        null
      );
    if (!diff) return lastState;
    return { ...lastState, ...diff };
  };

  return getStore().subscribe(() => {
    const currentState = getState();
    if (currentState !== lastState) {
      updateFunc(currentState);
      lastState = currentState;
    }
  });
};

export function reduxConnect(keys) {
  return function decorator(constructor) {
    let lastState = {};
    const getState = () => {
      const state = getStore().getState();
      const diff = keys
        .map(key => [keyName(key), getIn(state, keyPath(key))])
        .filter(([key, val]) => lastState[key] !== val)
        .reduce(
          (acc, [key, val]) => Object.assign(acc || {}, { [key]: val }),
          null
        );
      return diff ? { ...lastState, ...diff } : lastState;
    };

    return (...args) => {
      lastState = getState();
      const obj = new constructor(...args, lastState);

      const unsubscribe = getStore().subscribe(() => {
        const currentState = getState();
        if (currentState !== lastState) {
          obj.update(currentState);
          lastState = currentState;
        }
      });

      const oldUnmount = (obj.unmount || (() => {})).bind(obj);
      obj.unmount = () => {
        unsubscribe();
        oldUnmount();
      };

      return obj;
    };
  };
}

export function mount(...objs) {
  return function decorator(target) {
    return (...args) => {
      const instances = objs.map(obj => new obj());
      const obj = new target(...args);
      instances.forEach(i => obj.html.appendChild(i.html));
      const oldUnmount = (obj.unmount || (() => {})).bind(obj);
      obj.unmount = () => {
        instances.forEach(i => (i.unmount ? i.unmount() : null));
        oldUnmount();
      };
      return obj;
    };
  };
}
