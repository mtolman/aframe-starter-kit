import { getStore } from "./store";
import * as actions from './actions'

const cmd = func => (...args) => getStore().dispatch(func(...args))

export const moveBoxZ = cmd(actions.moveBoxZ)
export const moveSphereX = cmd(actions.moveSphereX)
export const restoreState = cmd(actions.restoreState)
