import { transformState as ts } from './store';
import { restoreState } from './commands';
import { Scene } from './scene';

const keyHandle = keyEvent => {
  keyEvent.preventDefault()
  keyEvent.stopPropagation()
  return false
}

export const App = function(rootElem, state) {
  if (state) restoreState(state);

  const app = document.getElementById(rootElem);
  app.innerHTML = '';
  const scene2 = new Scene()
  scene2.unmount()
  const scene = new Scene()
  app.appendChild(scene.html);

  document.addEventListener('keydown', keyHandle);
  this.unmount = () => {
    document.removeEventListener('keydown', keyHandle)
    scene.unmount()
  }
};

export const transformState = ts;
