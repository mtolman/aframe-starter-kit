import { combineReducers } from 'redux';
import { MOVE_BOX_Z, MOVE_SPHERE_X, RESTORE_STATE } from './actions';

function boxPos(state = [-1, 0.5, -3], action) {
  switch (action.type) {
    case MOVE_BOX_Z:
      return [state[0], state[1], state[2] - action.payload.amount];
    default:
      return state;
  }
}

function spherePos(state = [0, 1.25, -5], action) {
  switch (action.type) {
    case MOVE_SPHERE_X:
      return [state[0] - action.payload.amount, state[1], state[2]];
    default:
      return state;
  }
}

function schemaVersion(state = 1, action) {
  return state;
}

const rootReducer = (state, action) => {
  if(action.type === RESTORE_STATE)
    return action.payload

  return combineReducers({
    boxPos,
    spherePos,
    schemaVersion
  })(state, action);
};

export default rootReducer;
