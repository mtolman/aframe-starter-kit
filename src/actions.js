export const MOVE_BOX_Z = 'MOVE_Z'
export const RESTORE_STATE = 'RESTORE_STATE'
export const MOVE_SPHERE_X = 'MOVE_SPHERE_X'

export function moveBoxZ(amount) {
  return {
    type: MOVE_BOX_Z,
    payload: {amount}
  }
}

export function moveSphereX(amount) {
  return {
    type: MOVE_SPHERE_X,
    payload: {amount}
  }
}

export function restoreState(state) {
  return {
    type: RESTORE_STATE,
    payload: state
  }
}
