import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
function configureStore() {
  return createStore(rootReducer, composeEnhancers(applyMiddleware()))
}

const store = configureStore()

export function getStore() {
  return store
}

window.getState = getStore().getState.bind(getStore())

export function transformState(oldState) {
  return oldState
}
