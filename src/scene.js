import { p, r, c, reduxConnect, mount } from './utils';
import { moveBoxZ } from './commands';

function PositionedBox(positionRef) {
  return reduxConnect([positionRef])(function(state) {
    const box = document.createElement('a-box');
    box.setAttribute('position', p(state[positionRef]));
    box.setAttribute('rotation', r([0, 45, 0]));
    box.setAttribute('color', c('4CC3D9'));
    box.addEventListener('mouseenter', () =>
      box.setAttribute('color', c('8FF7FF'))
    );
    box.addEventListener('mouseleave', () =>
      box.setAttribute('color', c('4CC3D9'))
    );
    box.addEventListener('click', () => moveBoxZ(0.5));
    this.html = box;

    this.update = state => box.setAttribute('position', p(state[positionRef]));
  });
}

function PositionedSphere(positionRef) {
  return reduxConnect([positionRef, { path: 'boxPos.2', as: 'p1' }])(function(
    state
  ) {
    const sphere = document.createElement('a-sphere');
    sphere.setAttribute('color', '#EF2D5E');
    this.html = sphere;

    this.update = ({ spherePos, p1 }) => {
      sphere.setAttribute('position', p(spherePos));
      sphere.setAttribute('radius', Math.abs(p1 / 10) || 0.1);
    };

    this.update(state);
  });
}

function Camera() {
  const camera = document.createElement('a-camera');
  camera.setAttribute('position', p([0, 1.6, 0]));

  const cursor = document.createElement('a-entity');
  const scale = '0.005';
  const smScale = '0.001';
  cursor.setAttribute(
    'animation__click',
    `property: scale; startEvents: click; easing: easeInCubic; dur: 150; from: ${smScale} ${smScale} ${smScale}; to: ${scale} ${scale} ${scale}`
  );
  cursor.setAttribute(
    'animation__fusing',
    `property: scale; startEvents: fusing; easing: easeInCubic; dur: 1500; from: ${scale} ${scale} ${scale}; to: ${smScale} ${smScale} ${smScale}`
  );
  cursor.setAttribute(
    'animation__mouseleave',
    `property: scale; startEvents: mouseleave; easing: easeInCubic; dur: 500; to: ${scale} ${scale} ${scale}`
  );
  cursor.setAttribute('cursor', 'fuse: true');
  cursor.setAttribute('material', 'color: black; shader: flat');
  cursor.setAttribute('position', '0 0 -.1');
  cursor.setAttribute('scale', `${scale} ${scale} ${scale}`);
  cursor.setAttribute('geometry', 'primitive: ring');
  camera.appendChild(cursor);

  this.html = camera;
}

function Controls(hand) {
  return function() {
    const controls = document.createElement('a-entity');
    controls.setAttribute('hand-controls', hand);
    this.html = controls;
  };
}

function _Scene() {
  const scene = document.createElement('a-scene');

  const cylinder = document.createElement('a-cylinder');
  cylinder.setAttribute('position', '1 0.75 -3');
  cylinder.setAttribute('radius', '0.5');
  cylinder.setAttribute('height', '1.5');
  cylinder.setAttribute('color', '#FFC65D');
  cylinder.addEventListener('click', () => moveBoxZ(-0.5));
  scene.appendChild(cylinder);

  const plane = document.createElement('a-plane');
  plane.setAttribute('position', '0 0 -4');
  plane.setAttribute('rotation', '-90 0 0');
  plane.setAttribute('width', '4');
  plane.setAttribute('height', '4');
  plane.setAttribute('color', '#7BC8A4');
  scene.appendChild(plane);

  const sky = document.createElement('a-sky');
  sky.setAttribute('color', '#ECECEC');
  scene.appendChild(sky);

  this.updateMinimal = ({ boxPos, spherePos }) => {
    if (boxPos) box.setAttribute('position', p(boxPos));
    if (spherePos) sphere.setAttribute('position', p(spherePos));
  };

  this.html = scene;
}

export const Scene =
  _Scene
  |> mount(PositionedBox('boxPos'))
  |> mount(PositionedSphere('spherePos'))
  |> mount(PositionedBox('spherePos'))
  |> mount(Controls('left'))
  |> mount(Controls('right'))
  |> mount(Camera);
