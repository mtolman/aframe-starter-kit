import 'aframe'
let app = null
let state = null

if (module && module.hot) {
  module.hot.dispose(function(){
    backupState()
    if (app && app.unmount) {
      app.unmount()
    }
  })

  module.hot.accept(function(){
    restoreState()
  })
}

function backupState() {
  if (app && app.state && state.getState)
    window.state = state
  else
    delete window.state
}

function restoreState() {
  state = window.state
}

import('./app').then(({App, transformState}) => {
 app = new App('app', transformState(state))
})
